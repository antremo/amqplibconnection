const amqp = require("amqplib");

let createConnection = async (stringConnection) => {
  try {
    let connection = await amqp.connect(stringConnection);
    console.log("[AMQP] - Connected");

    connection.on("error", function () {
      console.error("[AMQP] reconnecting");

      connection.close();

      return setTimeout(() => {
        createConnection(stringConnection);
      }, 1000);
    });

    connection.on("close", function () {
      console.error("[AMQP] reconnecting");
      return setTimeout(() => {
        createConnection(stringConnection);
      }, 1000);
    });

    return connection;
  } catch (err) {
    return setTimeout(() => {
      console.log("[AMQP] Reintent");
      createConnection(stringConnection);
    }, 1000);
  }
};

module.exports = createConnection;
